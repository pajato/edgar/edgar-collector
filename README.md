# The Edgar coverage tool data collector

This project provides a data collector/runner to handle the lone use case of running tests, benchmarks and/or sample code on an insturmented project in order to generate a ocverage report for some project.

## Use cases

tbc

## Structure

tbc

## Building

Install the deployable (edgar-collector-<version>.jar to the local maven repoository using the Gradle task: *publishToMavenLocal* where it will be accessed by Edgar sub-projects which depend on the coverage data collector.
